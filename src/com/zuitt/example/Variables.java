// A "package" in Java is used to group related classes. Think of it as a folder in directory
// Packages are divided into two categories
    // 1. Built-in Packages - (packages from JAVA API)
    // 2. User-defined Packages
// Package creation follow the "reverse domain name notation" for the naming convention
package com.zuitt.example;

public class Variables {
    public static void main(String[] args) {

        //Variables
        int age;
        char middleInitial;

        //Variable Declaration vs. Initialization
        int x;
        int y = 0;

        //Initialization after declaration
        x = 1;

        //output to the systems
        System.out.println("The value of y is " + y + " and the value of x is " + x);

        //Primitive Data Types
        //predefined within the Java programming which is used for a "single-value" variable with limited capabilities

        //int - whole number values
        int wholeNumber = 100;
        System.out.println(wholeNumber);

        //long
        //L is being added at the end of the long number to be recognized
        long worldPopulation = 786514148556L;
        System.out.println(worldPopulation);


        //float
        // add f at the end of the float to be recognized
        float piFloat = 3.14159265359f;
        System.out.println(piFloat);

        //double
        double piDouble = 3.14159265359;
        System.out.println(piDouble);

        //char - single character
        char letter = 'a';
        System.out.println(letter);

        //boolean true of false
        boolean isLove = true;
        boolean isTaken = false;
        System.out.println(isLove);
        System.out.println(isTaken);

        isTaken = true;

        System.out.println(isTaken);

        //Constants
        final int PRINCIPAL = 3000;
        System.out.println(PRINCIPAL);

        //Non-primitive data type
            //also known as reference data types refer to instances or objects

        //String
        //Stores a sequences or array of characters
        //String are actually object that can use methods

        String userName = "JSmith";
        System.out.println(userName);

        int stringLength = userName.length();

        System.out.println(stringLength);




    }
}
