package com.zuitt.example;

import java.util.Scanner;
public class TypeConversion {
    public static void main(String[] args){
        Scanner myObj = new Scanner(System.in);
        System.out.println("Enter numbers to add. ");
        System.out.println("Enter a first number: ");

        //int num1 = Integer.parseInt(myObj.nextLine());
        int num1 = myObj.nextInt();

        System.out.println("Enter second number");
        int num2 = myObj.nextInt();

        System.out.println("The sum of two numbers is " + (num1 + num2));

        //nextInt() - expects an integer
        //nextDouble - expects a double
        //nextLine - gets the entire line as a String

    }
}

